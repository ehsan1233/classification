<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassGroupValue4CorpusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_group_values_4_corpus', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->timestamps();
			$table->unsignedBigInteger('class_group_value_id');
			$table->unsignedBigInteger('row_corpus_id');
			$table->foreign('class_group_value_id')->references('id')->on('class_group_values');
			$table->foreign('row_corpus_id')->references('id')->on('row_corpus');

		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('class_group_values_4_corpus');
    }
}
