<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RowCorpus extends Model
{
	protected $table = 'row_corpus';

	protected $fillable = [
		'text', 'corpus_id', 'is_labeled'
	];

	public function Corpus(){
		return $this->belongsTo(Corpus::class ,'corpus_id','id');
	}

}
