<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassGroups extends Model
{
	protected $table = 'class_groups';

	protected $fillable = [
		'name','account_id'
	];

	public function ClassGroupValues(){
		return $this->hasMany(ClassGroupValues::class,'group_id','id');
	}


}
