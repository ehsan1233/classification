<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Corpus extends Model
{
    protected $table = 'corpus';


	public function RowCorpus(){
		return $this->hasMany(RowCorpus::class,'corpus_id','id');
	}

}
