<?php

namespace App\Admin\Controllers;

use App\ClassGroups;
use App\ClassGroupValues;
use App\Corpus;
use App\RowCorpus;
use App\User;
use Encore\Admin\Controllers\Dashboard;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Encore\Admin\Show;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LabelingController extends Controller
{
	public function index(Content $content)
	{

		$grid = Admin::grid(RowCorpus::class, function (Grid $grid){
			$grid->column('text');
			$grid->Corpus(trans('corpus item'))->pluck('Class')->label();
		});
		$grid->filter(function($filter){

			// Remove the default id filter
			$filter->disableIdFilter();

			// Add a column filter
			$filter->like('name', 'name');

		});


		return $content
			->header('Labeling Items')
			->description('Labeling Item management')
			->body($grid);
	}

	public function create(Content $content){

		$grid = Admin::form(RowCorpus::class, function(Form $form){

			$form->text('text', 'row item');
		});



		return $content
			->header('Classes')
			->description('Class management')
			->body($grid);
	}

	public function update(Request $request,$id){

		$classGroup = ClassGroups::find($id);
		$classGroup->name = $request->get('name');

		$classGroup->save();

		return redirect('/admin/labeling');
	}

	public function store(Request $request){

		$classGroup = new RowCorpus([
			'text' => $request->get('text'),
			'corpus_id' => 1,
			'is_labeled' => 0,
		]);
		$classGroup->save();

		$groupValues = $request->get('groupValues');
		$groupValues = explode(',',$groupValues);
		foreach ($groupValues as $groupValue){
			$name = rtrim(ltrim($groupValue));
			if(strlen($name) === 0){
				continue;
			}
			$gg = new ClassGroupValues();
			$gg->name = $name;
			$gg->group_id = $classGroup->id;
			$gg->save();
		}

		return redirect('/admin/labeling');
	}

	public function edit(int $id,Content $content)
	{
		$form = new Form(new RowCorpus());

		$form->text('name', trans('name'))->rules('required');

		$form->saved(function () {
			admin_toastr(trans('successfully saved'));

			return redirect('/admin/classes');
		});

		return $content
			->header('Classes')
			->description('Class management')
			->body($form->edit($id));
	}

	public function destroy(int $id)
	{
		$classGroup = ClassGroups::find($id);
		if ($classGroup !== null) {
			$classGroup->delete();
		}

		return redirect('/admin/classes');
	}

	public function show(int $id, Content $content)
	{

		$show = new Show(ClassGroups::findOrFail($id));

		$show->id('ID');
		$show->name(trans('admin.name'));
		$show->ClassGroupValues(trans('group values'))->as(function ($groupValues) {
			return $groupValues->pluck('name');
		})->label();

		$show->created_at(trans('admin.created_at'));
		$show->updated_at(trans('admin.updated_at'));

		return $content
			->header('Classes')
			->description('Class management')
			->body($show);

	}
}
