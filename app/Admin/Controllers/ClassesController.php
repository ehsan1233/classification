<?php

namespace App\Admin\Controllers;

use App\ClassGroups;
use App\ClassGroupValues;
use App\User;
use Encore\Admin\Controllers\Dashboard;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Encore\Admin\Show;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ClassesController extends Controller
{
	public function index(Content $content)
	{

		$grid = Admin::grid(ClassGroups::class, function (Grid $grid){
			$grid->column('name');
			$grid->ClassGroupValues(trans('group values'))->pluck('name')->label();
		});
		$grid->filter(function($filter){

			// Remove the default id filter
			$filter->disableIdFilter();

			// Add a column filter
			$filter->like('name', 'name');

		});


		return $content
			->header('Classes')
			->description('Class management')
			->body($grid);
	}

	public function create(Content $content){

		$grid = Admin::form(ClassGroups::class, function(Form $form){

			$form->text('name', 'Class name');
			$form->text('groupValues', 'Sub class');

		});



		return $content
			->header('Classes')
			->description('Class management')
			->body($grid);
	}

	public function update(Request $request,$id){

		$classGroup = ClassGroups::find($id);
		$classGroup->name = $request->get('name');

		$classGroup->save();

		return redirect('/admin/classes');
	}

	public function store(Request $request){

		$classGroup = new ClassGroups([
			'name' => $request->get('name'),
			'account_id' => Admin::user()->account_id
		]);
		$classGroup->save();

		$groupValues = $request->get('groupValues');
		$groupValues = explode(',',$groupValues);
		foreach ($groupValues as $groupValue){
			$name = rtrim(ltrim($groupValue));
			if(strlen($name) === 0){
				continue;
			}
			$gg = new ClassGroupValues();
			$gg->name = $name;
			$gg->group_id = $classGroup->id;
			$gg->save();
		}

		return redirect('/admin/classes');
	}

	public function edit(int $id,Content $content)
	{
		$form = new Form(new ClassGroups());

		$form->text('name', trans('name'))->rules('required');

		$form->saved(function () {
			admin_toastr(trans('successfully saved'));

			return redirect('/admin/classes');
		});

		return $content
			->header('Classes')
			->description('Class management')
			->body($form->edit($id));
	}

	public function destroy(int $id)
	{
		$classGroup = ClassGroups::find($id);
		if ($classGroup !== null) {
			$classGroup->delete();
		}

		return redirect('/admin/classes');
	}

	public function show(int $id, Content $content)
	{

		$show = new Show(ClassGroups::findOrFail($id));

		$show->id('ID');
		$show->name(trans('admin.name'));
		$show->ClassGroupValues(trans('group values'))->as(function ($groupValues) {
			return $groupValues->pluck('name');
		})->label();

		$show->created_at(trans('admin.created_at'));
		$show->updated_at(trans('admin.updated_at'));

		return $content
			->header('Classes')
			->description('Class management')
			->body($show);

	}
}
