<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassGroupValues extends Model
{
	protected $table = 'class_group_values';

	public function ClassGroups(){
		return $this->belongsTo(ClassGroups::class ,'group_id','id');
	}

}
