<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassGroupValue4RowCorpus extends Model
{
	protected $table = 'class_group_value_4_row_corpus';


	public function ClassGroups(){
		return $this->belongsTo(ClassGroups::class ,'group_id','id');
	}


}
